use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn main() {
    println!("Result Part1: {}",part1());
    println!("Result Part2: {}",part2());
}


fn part1() -> i32{
    let mut number: i32 = 0;
    if let Ok(lines) = read_lines("./input") {
        for line in lines {
            if let Ok(mut line) = line {
                line.retain(char::is_numeric);
                number += (line.chars().next().unwrap() as i32 - '0' as i32) * 10;
                number += line.chars().last().unwrap() as i32 - '0' as i32;
            }
        }
    }
    return number;
}


fn part2() -> i32{
    let mut number: i32 = 0;
    if let Ok(lines) = read_lines("./input") {
        // Consumes the iterator, returns an (Optional) String
        for line in lines {
            if let Ok(mut line) = line {
                // Replacing with <name><int><name> to allow for overlapping words
                line = line.replace("one", "one1one");
                line = line.replace("two", "two2two");
                line = line.replace("three", "three3three");
                line = line.replace("four", "four4four");
                line = line.replace("five", "five5five");
                line = line.replace("six", "six6six");
                line = line.replace("seven", "seven7seven");
                line = line.replace("eight", "eight8eight");
                line = line.replace("nine", "nine9nine");
                line.retain(char::is_numeric);
                number += (line.chars().next().unwrap() as i32 - '0' as i32) * 10;
                number += line.chars().last().unwrap() as i32 - '0' as i32;
            }
        }
    }
    return number;
}


// https://doc.rust-lang.org/stable/rust-by-example/std_misc/file/read_lines.html
// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}
