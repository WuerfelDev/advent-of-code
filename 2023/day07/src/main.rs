use std::cmp::Ordering;
use std::collections::HashMap;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn main() {
    println!("Result Part1: {}", part1());
    println!("Result Part2: {}", part2());
}

fn order_of_chars(c: char) -> u32 {
    return match c {
        'A' => 0, // Strongest (when sorting first)
        'K' => 1,
        'Q' => 2,
        'J' => 3,
        'T' => 4,
        _ => 14 - c.to_digit(10).unwrap(),
    };
}

fn my_sort(a: &(String, u32), b: &(String, u32)) -> Ordering {
    let mut i = 0;
    while order_of_chars(a.0.chars().nth(i).unwrap()) == order_of_chars(b.0.chars().nth(i).unwrap())
    {
        i += 1;
    }

    return order_of_chars(a.0.chars().nth(i).unwrap())
        .cmp(&order_of_chars(b.0.chars().nth(i).unwrap()));
}

fn part1() -> u64 {
    let mut number: u64 = 0;
    // let mut hands: Vec<(String, u32)> = Vec::new();
    let mut five: Vec<(String, u32)> = Vec::new();
    let mut four: Vec<(String, u32)> = Vec::new();
    let mut fullhouse: Vec<(String, u32)> = Vec::new();
    let mut three: Vec<(String, u32)> = Vec::new();
    let mut twopair: Vec<(String, u32)> = Vec::new();
    let mut onepair: Vec<(String, u32)> = Vec::new();
    let mut high: Vec<(String, u32)> = Vec::new();
    if let Ok(lines) = read_lines("./input") {
        for line in lines {
            if let Ok(line) = line {
                let current_value = line
                    .split_once(" ")
                    .map(|x| (x.0.to_string(), x.1.parse::<u32>().unwrap()))
                    .unwrap();

                let mut diff_cards: HashMap<char, u8> = HashMap::new();
                for c in current_value.0.chars() {
                    diff_cards.entry(c).and_modify(|e| *e += 1).or_insert(1);
                }
                match diff_cards.values().max().unwrap() {
                    5 => five.push(current_value),
                    4 => four.push(current_value),
                    3 => {
                        if diff_cards.values().count() == 2 {
                            fullhouse.push(current_value);
                        } else {
                            three.push(current_value);
                        }
                    }
                    2 => {
                        if diff_cards.values().count() == 3 {
                            twopair.push(current_value);
                        } else {
                            onepair.push(current_value);
                        }
                    }
                    1 => high.push(current_value),
                    _ => unreachable!(),
                }
            }
        }
    }

    five.sort_by(|a, b| my_sort(a, b));
    four.sort_by(|a, b| my_sort(a, b));
    fullhouse.sort_by(|a, b| my_sort(a, b));
    three.sort_by(|a, b| my_sort(a, b));
    twopair.sort_by(|a, b| my_sort(a, b));
    onepair.sort_by(|a, b| my_sort(a, b));
    high.sort_by(|a, b| my_sort(a, b));

    let mut multiplier = five.len()
        + four.len()
        + fullhouse.len()
        + three.len()
        + twopair.len()
        + onepair.len()
        + high.len()
        + 1;

    number += five
        .iter()
        .map(|x| {
            multiplier -= 1;
            (x.1 as u64) * (multiplier as u64)
        })
        .sum::<u64>();
    number += four
        .iter()
        .map(|x| {
            multiplier -= 1;
            (x.1 as u64) * (multiplier as u64)
        })
        .sum::<u64>();
    number += fullhouse
        .iter()
        .map(|x| {
            multiplier -= 1;
            (x.1 as u64) * (multiplier as u64)
        })
        .sum::<u64>();
    number += three
        .iter()
        .map(|x| {
            multiplier -= 1;
            (x.1 as u64) * (multiplier as u64)
        })
        .sum::<u64>();
    number += twopair
        .iter()
        .map(|x| {
            multiplier -= 1;
            (x.1 as u64) * (multiplier as u64)
        })
        .sum::<u64>();
    number += onepair
        .iter()
        .map(|x| {
            multiplier -= 1;
            (x.1 as u64) * (multiplier as u64)
        })
        .sum::<u64>();
    number += high
        .iter()
        .map(|x| {
            multiplier -= 1;
            (x.1 as u64) * (multiplier as u64)
        })
        .sum::<u64>();
    return number;
}

fn order_of_chars_p2(c: char) -> u32 {
    return match c {
        'A' => 0, // Strongest (when sorting first)
        'K' => 1,
        'Q' => 2,
        'J' => 13,
        'T' => 4,
        _ => 14 - c.to_digit(10).unwrap(),
    };
}

fn my_sort_p2(a: &(String, u32), b: &(String, u32)) -> Ordering {
    let mut i = 0;
    while order_of_chars_p2(a.0.chars().nth(i).unwrap())
        == order_of_chars_p2(b.0.chars().nth(i).unwrap())
    {
        i += 1;
    }

    return order_of_chars_p2(a.0.chars().nth(i).unwrap())
        .cmp(&order_of_chars_p2(b.0.chars().nth(i).unwrap()));
}

fn part2() -> u64 {
    let mut number: u64 = 0;
    // let mut hands: Vec<(String, u32)> = Vec::new();
    let mut five: Vec<(String, u32)> = Vec::new();
    let mut four: Vec<(String, u32)> = Vec::new();
    let mut fullhouse: Vec<(String, u32)> = Vec::new();
    let mut three: Vec<(String, u32)> = Vec::new();
    let mut twopair: Vec<(String, u32)> = Vec::new();
    let mut onepair: Vec<(String, u32)> = Vec::new();
    let mut high: Vec<(String, u32)> = Vec::new();
    if let Ok(lines) = read_lines("./input") {
        for line in lines {
            if let Ok(line) = line {
                let current_value = line
                    .split_once(" ")
                    .map(|x| (x.0.to_string(), x.1.parse::<u32>().unwrap()))
                    .unwrap();

                // Determine no. of different cards in the hand
                let mut diff_cards: HashMap<char, u8> = HashMap::new();
                for c in current_value.0.chars() {
                    diff_cards.entry(c).and_modify(|e| *e += 1).or_insert(1);
                }

                // Remove J and add it to highest existing
                let jokers = diff_cards.remove(&'J');
                if jokers.is_some() {
                    if diff_cards.is_empty() {
                        diff_cards.insert('J', 5);
                    } else {
                        // Sort tuples by value then add jokers
                        let mut sorted_tuples: Vec<(&char, &u8)> = diff_cards.iter().collect();
                        sorted_tuples.sort_by(|a, b| a.1.cmp(b.1));
                        let max_key = sorted_tuples.iter().last().unwrap().0;

                        diff_cards
                            .entry(*max_key)
                            .and_modify(|e| *e += jokers.unwrap());
                    }
                }
                match diff_cards.values().max().unwrap() {
                    5 => five.push(current_value),
                    4 => four.push(current_value),
                    3 => {
                        if diff_cards.values().count() == 2 {
                            fullhouse.push(current_value);
                        } else {
                            three.push(current_value);
                        }
                    }
                    2 => {
                        if diff_cards.values().count() == 3 {
                            twopair.push(current_value);
                        } else {
                            onepair.push(current_value);
                        }
                    }
                    1 => high.push(current_value),
                    _ => unreachable!(),
                }
            }
        }
    }

    five.sort_by(|a, b| my_sort_p2(a, b));
    four.sort_by(|a, b| my_sort_p2(a, b));
    fullhouse.sort_by(|a, b| my_sort_p2(a, b));
    three.sort_by(|a, b| my_sort_p2(a, b));
    twopair.sort_by(|a, b| my_sort_p2(a, b));
    onepair.sort_by(|a, b| my_sort_p2(a, b));
    high.sort_by(|a, b| my_sort_p2(a, b));

    let mut multiplier = five.len()
        + four.len()
        + fullhouse.len()
        + three.len()
        + twopair.len()
        + onepair.len()
        + high.len()
        + 1;

    number += five
        .iter()
        .map(|x| {
            multiplier -= 1;
            (x.1 as u64) * (multiplier as u64)
        })
        .sum::<u64>();
    number += four
        .iter()
        .map(|x| {
            multiplier -= 1;
            (x.1 as u64) * (multiplier as u64)
        })
        .sum::<u64>();
    number += fullhouse
        .iter()
        .map(|x| {
            multiplier -= 1;
            (x.1 as u64) * (multiplier as u64)
        })
        .sum::<u64>();
    number += three
        .iter()
        .map(|x| {
            multiplier -= 1;
            (x.1 as u64) * (multiplier as u64)
        })
        .sum::<u64>();
    number += twopair
        .iter()
        .map(|x| {
            multiplier -= 1;
            (x.1 as u64) * (multiplier as u64)
        })
        .sum::<u64>();
    number += onepair
        .iter()
        .map(|x| {
            multiplier -= 1;
            (x.1 as u64) * (multiplier as u64)
        })
        .sum::<u64>();
    number += high
        .iter()
        .map(|x| {
            multiplier -= 1;
            (x.1 as u64) * (multiplier as u64)
        })
        .sum::<u64>();
    return number;
}

// https://doc.rust-lang.org/stable/rust-by-example/std_misc/file/read_lines.html
// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}
