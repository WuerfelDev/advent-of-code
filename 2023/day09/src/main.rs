use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn main() {
    println!("Result Part1: {}", part1());
    println!("Result Part2: {}", part2());
}

fn part1() -> i64 {
    let mut number: i64 = 0;
    if let Ok(lines) = read_lines("./input") {
        for line in lines {
            if let Ok(line) = line {
                let mut collection: Vec<i64> = line
                    .split_whitespace()
                    .map(|x| x.parse::<i64>().unwrap())
                    .collect();
                loop {
                    if collection.iter().all(|x| x == collection.first().unwrap()) {
                        number += collection.first().unwrap();
                        break;
                    }
                    let mut new_collection: Vec<i64> = Vec::new();
                    for i in 1..collection.len() {
                        new_collection.push(collection[i] - collection[i - 1]);
                    }
                    number += collection.last().unwrap();
                    collection = new_collection;
                }
            }
        }
    }
    return number;
}

fn part2() -> i64 {
    let mut number: i64 = 0;
    if let Ok(lines) = read_lines("./input") {
        for line in lines {
            if let Ok(line) = line {
                let mut collection: Vec<i64> = line
                    .split_whitespace()
                    .map(|x| x.parse::<i64>().unwrap())
                    .collect();
                let mut alternate = true; // We get the previous number by building the alternative sum of the first row
                loop {
                    if collection.iter().all(|x| x == collection.first().unwrap()) {
                        number += (alternate as i64 * 2 - 1) * collection.first().unwrap();
                        break;
                    }
                    let mut new_collection: Vec<i64> = Vec::new();
                    for i in 1..collection.len() {
                        new_collection.push(collection[i] - collection[i - 1]);
                    }
                    number += (alternate as i64 * 2 - 1) * collection.first().unwrap();
                    alternate = !alternate;
                    collection = new_collection;
                }
            }
        }
    }
    return number;
}

// https://doc.rust-lang.org/stable/rust-by-example/std_misc/file/read_lines.html
// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}
