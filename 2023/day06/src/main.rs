use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn main() {
    println!("Result Part1: {}", part1());
    println!("Result Part2: {}", part2());
}

fn part1() -> u32 {
    let mut number: u32 = 1;
    if let Ok(mut lines) = read_lines("./input") {
        let timing: Vec<u32> = lines
            .nth(0)
            .unwrap()
            .unwrap()
            .split_once(": ")
            .unwrap()
            .1
            .trim_start()
            .split(" ")
            .filter(|x| x != &"")
            .map(|x| x.parse::<u32>().unwrap())
            .collect();

        let distance: Vec<u32> = lines
            .last()
            .unwrap()
            .unwrap()
            .split_once(": ")
            .unwrap()
            .1
            .trim_start()
            .split(" ")
            .filter(|x| x != &"")
            .map(|x| x.parse::<u32>().unwrap())
            .collect();

        for i in 0..timing.len() {
            let mut counter = 0;
            for n in 1..timing[i] {
                counter += (n * (timing[i] - n) > distance[i]) as u32;
            }
            number *= counter;
        }
    }
    return number;
}

fn part2() -> u64 {
    let mut number: u64 = 0;
    if let Ok(mut lines) = read_lines("./input") {
        let timing = lines
            .nth(0)
            .unwrap()
            .unwrap()
            .split_once(": ")
            .unwrap()
            .1
            .replace(" ", "")
            .parse::<u64>()
            .unwrap();

        let distance = lines
            .nth(0)
            .unwrap()
            .unwrap()
            .split_once(": ")
            .unwrap()
            .1
            .replace(" ", "")
            .parse::<u64>()
            .unwrap();

        let mut i_start = 1;
        while (i_start * (timing - i_start)) < distance {
            i_start += 1;
        }

        number = timing - i_start * 2 + 1;
    }
    return number;
}

// https://doc.rust-lang.org/stable/rust-by-example/std_misc/file/read_lines.html
// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}
