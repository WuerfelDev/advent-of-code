use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn main() {
    println!("Result Part1: {}",part1());
    println!("Result Part2: {}",part2());
}

/// Returns true if number is lower than its max reach
fn check_color_failed(linedata: &str) -> bool {
    for l in linedata.split("; ") {
        for c in l.split(", ") {
            let mut reach = 12; // 12 for red
            if c.contains("green") {
                reach = 13;
            }else if c.contains("blue") {
                reach = 14;
            }
            let mut block = c.to_string();
            block.retain(char::is_numeric);
            if block.parse::<i32>().unwrap() > reach {
                return true;
            }
        }
    }
    return false;
}


fn part1() -> i32{
    let mut number: i32 = 0;
    if let Ok(lines) = read_lines("./input") {
        for line in lines {
            if let Ok(line) = line {
                let parts= line.split_once(": ").unwrap();
                if !check_color_failed(parts.1) {
                    let mut p = parts.0.to_string();
                    p.retain(char::is_numeric);
                    number += p.parse::<i32>().unwrap();
                }
            }
        }
    }
    return number;
}


fn get_min_viable(linedata: &str) -> i64 {
    let mut min_values = [0i64;3]; // Max value entries are min viable
    for l in linedata.split("; ") {
        for c in l.split(", ") {
            let mut index = 0;
            if c.contains("green") {
                index = 1;
            }else if c.contains("blue") {
                index = 2;
            }
            let mut block = c.to_string();
            block.retain(char::is_numeric);
            let current_num = block.parse::<i64>().unwrap();
            if min_values[index] < current_num {
                min_values[index] = current_num;
            }
        }
    }
    return min_values.iter().product();
}


fn part2() -> i64{
    let mut number: i64 = 0;
    if let Ok(lines) = read_lines("./input") {
        for line in lines {
            if let Ok(line) = line {
                let parts= line.split_once(": ").unwrap();
                number += get_min_viable(parts.1);
            }
        }
    }
    return number;
}

// https://doc.rust-lang.org/stable/rust-by-example/std_misc/file/read_lines.html
// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}
