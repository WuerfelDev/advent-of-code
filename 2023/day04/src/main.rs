use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn main() {
    println!("Result Part1: {}",part1());
    println!("Result Part2: {}",part2());
}


fn part1() -> u32{
    let mut number: u32 = 0;
    if let Ok(lines) = read_lines("./input") {
        for line in lines {
            if let Ok(line) = line {
                let gamedata = line
                    .split_once(": ")
                    .unwrap().1
                    .split_once(" | ")
                    .unwrap();

                let mut winning_numbers: Vec<u32>= gamedata.0.split(" ").filter(|x| x!=&"").map(|x| x.parse::<u32>().unwrap()).collect();
                let my_numbers= gamedata.1.split(" ").filter(|x| x!=&"").map(|x| x.parse::<u32>().unwrap());
                let mut winning_num_count = 0u32;
                for n in my_numbers {
                    winning_num_count += winning_numbers.contains(&n) as u32;
                }
                if winning_num_count > 0 {
                    number += u32::pow(2, winning_num_count-1);
                }
            }
        }
    }
    return number;
}


fn part2() -> u64{
    let maxval = 201;
    let mut cards: Vec<u64> = vec![1u64; maxval];
    if let Ok(lines) = read_lines("./input") {
        for (lnr,line) in lines.enumerate() {
            if let Ok(line) = line {
                let gamedata = line
                    .split_once(": ")
                    .unwrap().1
                    .split_once(" | ")
                    .unwrap();

                let winning_numbers: Vec<u32> = gamedata.0
                    .split(" ")
                    .filter(|x| x!=&"")
                    .map(|x| x.parse::<u32>().unwrap())
                    .collect();
                let my_numbers: Vec<u32> = gamedata.1
                    .split(" ")
                    .filter(|x| x!=&"")
                    .map(|x| x.parse::<u32>().unwrap())
                    .collect();
                let mut winning_num_count = 0usize;
                for n in my_numbers {
                    winning_num_count += winning_numbers.contains(&n) as usize;
                }
                for _ in 0..cards[lnr] {
                    for i in 1..=winning_num_count {
                        cards[lnr+i as usize] += (lnr+i < maxval) as u64;
                    }
                }
            }
        }
    }
    return cards.iter().sum();
}


// https://doc.rust-lang.org/stable/rust-by-example/std_misc/file/read_lines.html
// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}
