use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

use itertools::Itertools;

fn main() {
    println!("Result Part1: {}", part1());
    println!("Result Part2: {}", part2());
}

fn part1() -> usize {
    let mut number = 0;
    let mut galaxies: Vec<(usize, usize, usize)> = Vec::new();
    let mut empty_cols: Vec<usize> = Vec::new();
    if let Ok(lines) = read_lines("./input") {
        let mut y = 0;
        let mut counter = 1;
        for line in lines {
            if let Ok(line) = line {
                if y == 0 {
                    empty_cols = line.match_indices(".").map(|(x, _)| x).collect();
                } else {
                    empty_cols = line
                        .match_indices(".")
                        .map(|(x, _)| x)
                        .filter(|x| empty_cols.contains(x))
                        .collect();
                }

                let mut nogalaxy = true;
                for x in line
                    .match_indices("#")
                    .map(|(x, _)| x)
                    .collect::<Vec<usize>>()
                {
                    nogalaxy = false;
                    galaxies.push((x, y, counter));
                    counter += 1;
                }
                y += 1 + 1 * (nogalaxy as usize); // Double the y value if no galaxies exist
            }
        }

        // At this point y was adjusted but x not yet
        for galaxy in galaxies.iter_mut() {
            let mut add = 0;
            for &empty_col in &empty_cols {
                if galaxy.0 > empty_col {
                    add += 1;
                }
            }
            galaxy.0 += add;
        }

        for (galaxy_a, galaxy_b) in galaxies.into_iter().tuple_combinations() {
            number += galaxy_a.0.abs_diff(galaxy_b.0) + galaxy_a.1.abs_diff(galaxy_b.1);
            // let diff = galaxy_a.0.abs_diff(galaxy_b.0) + galaxy_a.1.abs_diff(galaxy_b.1);
            // println!("diff {:?}-{:?} is {}", galaxy_a, galaxy_b, diff);
        }
    }
    return number;
}

fn part2() -> usize {
    let distance = 1000000;
    let mut number = 0;
    let mut galaxies: Vec<(usize, usize, usize)> = Vec::new();
    let mut empty_cols: Vec<usize> = Vec::new();
    if let Ok(lines) = read_lines("./input") {
        let mut y = 0;
        let mut counter = 1;
        for line in lines {
            if let Ok(line) = line {
                if y == 0 {
                    empty_cols = line.match_indices(".").map(|(x, _)| x).collect();
                } else {
                    empty_cols = line
                        .match_indices(".")
                        .map(|(x, _)| x)
                        .filter(|x| empty_cols.contains(x))
                        .collect();
                }

                let mut nogalaxy = true;
                for x in line
                    .match_indices("#")
                    .map(|(x, _)| x)
                    .collect::<Vec<usize>>()
                {
                    nogalaxy = false;
                    galaxies.push((x, y, counter));
                    counter += 1;
                }
                y += 1 + (distance - 1) * (nogalaxy as usize); // Double the y value if no galaxies exist
            }
        }

        // At this point y was adjusted but x not yet
        for galaxy in galaxies.iter_mut() {
            let mut add = 0;
            for &empty_col in &empty_cols {
                if galaxy.0 > empty_col {
                    add += distance - 1;
                }
            }
            galaxy.0 += add;
        }

        for (galaxy_a, galaxy_b) in galaxies.into_iter().tuple_combinations() {
            number += galaxy_a.0.abs_diff(galaxy_b.0) + galaxy_a.1.abs_diff(galaxy_b.1);
            // let diff = galaxy_a.0.abs_diff(galaxy_b.0) + galaxy_a.1.abs_diff(galaxy_b.1);
            // println!("diff {:?}-{:?} is {}", galaxy_a, galaxy_b, diff);
        }
    }
    return number;
}
// https://doc.rust-lang.org/stable/rust-by-example/std_misc/file/read_lines.html
// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}
