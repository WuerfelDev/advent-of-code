use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn main() {
    println!("Result Part1: {}", part1());
    // println!("Result Part2: {}",part2());
}

fn part1() -> i32 {
    let mut number: i32 = 0;
    if let Ok(lines) = read_lines("./input") {
        for line in lines {
            if let Ok(mut line) = line {}
        }
    }
    return number;
}

// https://doc.rust-lang.org/stable/rust-by-example/std_misc/file/read_lines.html
// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}
