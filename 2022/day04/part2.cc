#include <iostream>
#include <tuple>

int main(int argc, char *argv[]){
    int score = 0;
    std::string line;
    while (std::getline(std::cin,line)) {
        int split = line.find(',');
        std::string first = line.substr(0, split);
        std::string second = line.substr(split+1);

        int split_first = first.find('-');
        int split_second = second.find('-');

        int first_min = std::stoi(first.substr(0,split_first));
        int first_max = std::stoi(first.substr(split_first+1));

        int second_min = std::stoi(second.substr(0,split_second));
        int second_max = std::stoi(second.substr(split_second+1));

        if((first_min >= second_min && first_min <= second_max) ||
                (second_min >= first_min && second_min <= first_max) ||
                (first_max >= second_min && first_max <= second_max) ||
                (second_max >= first_min && second_max <= first_max)){
            // it's contained
            score++;
        }
    }
    printf("Result is: %d", score);
}
