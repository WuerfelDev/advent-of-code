#!/usr/bin/env bash

function show_help {
    echo $(basename $0) "[day] [challenge]"
    echo ''
    echo 'Parameter day:        Number of the day (required)'
    echo 'Parameter challenge:  Part of the challenge (either 1 or 2) (optional)'
    echo '                      by default both challenges are executed'
}


function run_code {
    input_file="input"
    # Check for existance of code and input
    #dir=$(dirname $)
    dir=$(dirname $BASH_SOURCE)
    if [ ! -f "$dir/day$1/part$2.cc" ] || [ ! -f "$dir/day$1/$input_file" ]; then
        echo "Day $1 Part $2: Source code or input file does not exist."
    else
        echo "----- Part $2 -----"
        pushd "$dir/day$1/" > /dev/null
        g++ "part$2.cc" -o "part$2"
        cat "$input_file" | "./part$2"
        echo '' # In case I don't end the output with \n 
        popd > /dev/null
    fi
}

if [ -z "$1" ]; then
    show_help
    exit 1
elif [ "$1" = "--help" ] || [ "$1" = "-h" ]; then
    show_help
    exit 0
fi

# Minium 2 chars width, fill with 0 padding in front if required
day=$(printf "%02i" $1)

if [ -z "$2" ]; then
    run_code $day 1
    run_code $day 2
elif [ "$2" = "1" ] || [ "$2" = "2" ]; then
    run_code $day $2
fi
