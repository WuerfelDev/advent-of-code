#include <iostream>
#include <algorithm>
#include <vector>

int main(int argc, char *argv[]){
    std::vector<int> calories (1,0);
    std::string line;
    while (std::getline(std::cin,line)) {
        if (line.size() == 0) {
            calories.push_back(0);
        }else{
            calories.back() += std::stoi(line);
        }
    }
    sort(calories.begin(), calories.end());
    int max = calories.back();
    for(int i = 0; i < 3-1; i++){
        calories.pop_back();
        max += calories.back();
    }
    printf("Result is: %d", max);
}
