#include <iostream>
#include <algorithm>

int main(int argc, char *argv[]){
    int max = 0;
    int current = 0;
    std::string line;
    while (std::getline(std::cin,line)) {
        if (line.size() == 0) {
            max = std::max(max,current);
            current = 0;
        }else{
            current += std::stoi(line);
        }
    }
    max = std::max(max,current);
    printf("Result is: %d", max);
}
