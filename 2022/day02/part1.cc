#include <iostream>

int main(int argc, char *argv[]){
    int score = 0;
    std::string line;
    while (std::getline(std::cin,line)) {
        char opponent = line.at(0);
        char me = line.at(2);

        int distance = 'X' - 'A';
        if (opponent+distance == me){
            // Draw
            score += 3;
        }else if( (opponent=='A' && me=='Y') ||
                ( opponent=='B' && me=='Z') ||
                ( opponent=='C' && me=='X') ){
            // I win
            score += 6;
        }

        // 1,2,3 points for the choice
        score += me - 'X' + 1;
    }
    printf("Result is: %d", score);
}
