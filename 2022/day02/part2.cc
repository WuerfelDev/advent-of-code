#include <iostream>

int main(int argc, char *argv[]){
    int score = 0;
    std::string line;
    while (std::getline(std::cin,line)) {
        char opponent = line.at(0);
        char outcome = line.at(2);

        score += (outcome - 'X') * 3;

        if (outcome=='Y'){
            score += opponent - 'A' + 1;
        }else if ((outcome=='X' && opponent=='A') ||
                (outcome=='Z' && opponent=='B')) {
            //Scissors
            score += 3;
        }else if( (outcome=='X' && opponent=='B') ||
                (outcome=='Z' && opponent=='C')){
            // Rock
            score += 1;
        }else{
            // Paper
            score += 2;
        }

    }
    printf("Result is: %d", score);
}
