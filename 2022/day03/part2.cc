#include <iostream>
#include <vector>

int main (int argc, char *argv[]){
    int result = 0;
    std::string alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    std::vector<std::string> otherlines;
    std::string line;
    while (std::getline(std::cin,line)){
        if(otherlines.size() != 2){
            otherlines.push_back(line);
        }else{
            // search through every character of current line
            for(int i=0;i<line.size();i++){
                // find in both other backpacks
                if(otherlines[0].find_first_of(line.at(i)) != std::string::npos && otherlines[1].find_first_of(line.at(i)) != std::string::npos){
                    result += alphabet.find_first_of(line.at(i)) + 1;
                    otherlines.clear();
                    //printf("found %c\n",line.at(i));
                    break;
                }
            }
        }
    }

    printf("Result is: %i", result);
}
