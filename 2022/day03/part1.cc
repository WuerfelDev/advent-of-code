#include <iostream>

int main (int argc, char *argv[]){
    int result = 0;
    std::string alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    std::string line;
    while (std::getline(std::cin,line)){
        int compartment = line.size()/2;
        for(int i=0;i<compartment;i++){
            // Search every char of first compartment
            int pos = line.find_last_of(line.at(i));
            if(pos >= compartment){
                // Found in second compartment
                result += alphabet.find_first_of(line.at(i)) + 1;
                //printf("Found %c, %d\n",line.at(i),result);
                break;
            }
        }
    }

    printf("Result is: %i", result);
}
