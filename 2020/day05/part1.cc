#include <iostream>
#include <cmath> 


int seat_pos(std::string pos){
    if(pos.length()>0){
        int add = 0;
        if(pos[0] == 'B' || pos.front() == 'R'){
            add = pow(2,(pos.length()-1));
        }
        return add + seat_pos(pos.substr(1));
    }
    return 0;
}

int main(int argc, char *argv[]){
    std::string line;
    int answer = 0;
    while (std::getline(std::cin,line)) {
        if(line!=""){
            int row = seat_pos(line.substr(0,7));
            int column = seat_pos(line.substr(7,9));
            int id = row * 8 + column;
            if(id > answer){
                answer = id;
            }
        }
    }
    
    printf("Highest seat ID: %d\n", answer);
}
