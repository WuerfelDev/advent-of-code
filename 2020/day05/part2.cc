#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>


int seat_pos(std::string pos){
    if(pos.length()>0){
        int add = 0;
        if(pos[0] == 'B' || pos.front() == 'R'){
            add = pow(2,(pos.length()-1));
        }
        return add + seat_pos(pos.substr(1));
    }
    return 0;
}

int main(int argc, char *argv[]){
    std::string line;
    std::vector<int> collector;
    int myid = 0;
    while (std::getline(std::cin,line)) {
        if(line!="" && myid==0){
            int row = seat_pos(line.substr(0,7));
            int column = seat_pos(line.substr(7,9));
            int id = row * 8 + column;
            collector.push_back(id);
        }
    }
    std::sort(collector.begin(), collector.end());
    for(int i=0;i<collector.size()&&myid==0;i++){
        if(collector[i]!=collector[i+1]-1){
            myid= collector[i]+1;
        }
    }


    printf("My seat ID: %d\n", myid);
}
