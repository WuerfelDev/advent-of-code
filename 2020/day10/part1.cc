#include <iostream>
#include <vector>
#include <algorithm>


int main(int argc, char *argv[]){
    std::string line;
    // Default outlet joltage
    std::vector<int> joltages = { 0 };
    while (std::getline(std::cin,line)) {
        joltages.push_back(stoi(line));
    }

    sort(joltages.begin(), joltages.end());
    
    int diff_one = 0;
    int diff_two = 0; // This case never happens
    int diff_three = 1; // Device has a joltage 3 higher than largest adapter
    for(int i=1;i<joltages.size();i++){
        int diff = joltages[i]-joltages[i-1];
        diff_one += diff==1;
        diff_two += diff==2;
        diff_three += diff==3;
    }
    

    printf("Joltage diffs multiplied: %d\n",diff_one*diff_three);
}
