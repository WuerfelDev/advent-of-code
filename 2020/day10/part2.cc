#include <iostream>
#include <vector>
#include <algorithm>




/*// This works fine but takes forever
long long adapter_arrangement(std::vector<int> joltages){
    if(joltages.empty() || joltages.size()==1){
        return 1; //reached a possible end
    }else{
        int current = joltages.front();
        long long res = 0;
        for(int i=1;i<joltages.size() && joltages[i]-current<=3;i++){
            std::vector<int> new_vector(joltages.begin()+i, joltages.end());
            res += adapter_arrangement(new_vector);
        }
        return res;
    }
}*/


// Inspired by https://github.com/arknave/advent-of-code-2020/blob/main/day10/day10.py
long long adapter_arrangement(std::vector<int> joltages){
    std::vector<long long> v = { 1 };
    for(int i=1;i<joltages.size();i++){
        long long ret = 0;
        // Add together the possibilities from every possible previous adapter
        // j is elements smaller than i. As long as the value diff is <=3
        for(int j=0;j<i;j++){
            if(joltages[i]-joltages[j]<=3){
                ret += v[j];
            }
        }
        v.push_back(ret);
    }
    return v.back();
}


int main(int argc, char *argv[]){
    std::string line;
    // Default outlet joltage
    std::vector<int> joltages = { 0 };
    while (std::getline(std::cin,line)) {
        joltages.push_back(stoi(line));
    }

    sort(joltages.begin(), joltages.end());

    long long res = adapter_arrangement(joltages);

    printf("Adapter possibilities: %lld\n",res);
}
