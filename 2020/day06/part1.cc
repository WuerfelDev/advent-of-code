#include <iostream>


int main(int argc, char *argv[]){
    std::string line;
    std::string answers;
    int result = 0;
    while (std::getline(std::cin,line)) {
        if(line!=""){
            for(int i=0;i<line.length();i++){
                if(answers.find(line[i])==std::string::npos){
                    answers += line[i];
                }
            }
        }else{
            result += answers.length();
            answers.clear();
        }
    }
    //Last line is not empty so we need to add it to result too
    result += answers.length();
    printf("Result: %d\n", result);
}
