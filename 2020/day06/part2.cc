#include <iostream>


int main(int argc, char *argv[]){
    std::string line;
    std::string answers;
    int result = 0;
    bool first_person = true;
    while (std::getline(std::cin,line)) {
        if(!line.empty()){
            if(first_person){
                answers = line;
                first_person = false;
            }else{
                std::string new_answers;
                // Create new answers string that excludes the non-same letters
                for(int i=0;i<line.length();i++){
                    if(answers.find(line[i])!=std::string::npos){
                        new_answers += line[i];
                    }
                }
                answers = new_answers;
            }
        }else{
            result += answers.length();
            answers.clear();
            first_person = true;
        }
    }
    //Last line is not empty so we need to add it to result too
    result += answers.length();
    printf("Result: %d\n", result);
}
