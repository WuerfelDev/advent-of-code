#include <iostream>
#include <vector>


int slope(std::vector<std::string> landscape, int right, int down){
    int trees = 0;
    int counter = 0;
    for(int i=0;i<landscape.size();i+=down){
        std::string line = landscape[i];
        if(line[(counter*right)%line.size()]=='#'){
            trees++;
        }
        counter++;
    }
    printf("%d,%d: %d\n",right,down,trees);
    return trees;
}


int main(int argc, char *argv[]){
    std::string line;
    std::vector<std::string> landscape;
    while (std::getline(std::cin,line)) {
        landscape.push_back(line);
    }

    long int answer = slope(landscape,1,1);
    answer *= slope(landscape,3,1);
    answer *= slope(landscape,5,1);
    answer *= slope(landscape,7,1);
    answer *= slope(landscape,1,2);

    printf("Answer: %ld\n", answer);
}
