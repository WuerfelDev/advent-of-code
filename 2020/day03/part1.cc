#include <iostream>


int main(int argc, char *argv[]){
    std::string line;
    int trees = 0;
    int counter = 0;
    while (std::getline(std::cin,line)) {
        if(line[(counter*3)%line.size()]=='#'){
            trees++;
        }
        counter++;
    }
    printf("Trees: %d\n", trees);
}
