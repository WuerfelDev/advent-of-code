#include <iostream>
#include <vector>

bool is_addition(std::vector<long long> numbers,long long current){
    int len = numbers.size();
    for(int i=len-25;i<len;i++){
        for(int j=i;j<len;j++){
            if(numbers[i]+numbers[j]==current){
                return true;
            }
        }
    }
    return false;
}


int main(int argc, char *argv[]){
    std::string line;
    std::vector<long long> numbers;
    int pos = 0;
    while (std::getline(std::cin,line)) {
        long long current = stoll(line);
        if(pos>=25){
            if(!is_addition(numbers,current)){
                printf("Found mismatch at %d: %lld\n",pos, current);    
                return 0;
            }
        }
        pos++;
        numbers.push_back(current);
    }
    printf("All numbers seem fine");
}
