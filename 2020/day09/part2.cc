#include <iostream>
#include <vector>


bool added_up_is_equal(std::vector<long long> numbers,long long search){
    int len = numbers.size();
    long long calc = numbers.back();
    for(int i=len-2;i>=0;i--){
        calc += numbers[i];
        if(calc==search){
            long long minn = numbers[i];
            long long maxn = numbers[i];
            for(int j=i+1;j<len;j++){
                if(minn>numbers[j]){
                    minn = numbers[j];
                }
                if(maxn<numbers[j]){
                    maxn = numbers[j];
                }
            }
            printf("Result is %lld",minn+maxn);
            return true;
        }else if(calc>search){
            // calc got too big
            return false;
        }
    }
    // Number stayed below search
    return false;
}


int main(int argc, char *argv[]){
    std::string line;
    std::vector<long long> numbers;
    long long search = 15353384;
    while (std::getline(std::cin,line)) {
        numbers.push_back(stoll(line));
        if(added_up_is_equal(numbers,search)){
            return 0;
        }
    }

    printf("Couldnt find a match");
}
