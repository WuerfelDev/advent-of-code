#include <iostream>
#include <vector>


void printvector(std::vector<std::string> rows){
    for(int i=0;i<rows.size();i++){
        printf("%s\n",rows[i].c_str());
    }
    printf("\n");
}


int get_occupied(std::vector<std::string> rows,int pos_i,int pos_j){
    int ret = 0;
    for(int i=-1;i<=1;i++){
        for(int j=-1;j<=1;j++){
            if(!(i==0&&j==0)){
                int distance = 1;
                while(1){
                    int new_i = pos_i+(i*distance);
                    int new_j = pos_j+(j*distance);
                    if(new_i<0 || new_i>=rows.size() || new_j<0 || new_j>=rows[i].size()){
                        break;
                    }
                    if(rows[new_i][new_j]!='.'){
                        ret += rows[new_i][new_j]=='#';
                        break;
                    }
                    distance++;
                }
            }
        }
    }
    return ret;
}


std::vector<std::string> simulate(std::vector<std::string> rows){
    std::vector<std::string> cpy(rows);
    for(int i=0;i<rows.size();i++){
        for(int j=0;j<rows[i].size();j++){
            if(rows[i][j]!='.'){
                int occupied = get_occupied(rows,i,j);
                if(rows[i][j]=='L' && occupied==0){
                    cpy[i][j] = '#';
                }else if(rows[i][j]=='#' && occupied>=5){
                    cpy[i][j] = 'L';
                }
            }
        }
    }
    return cpy;
}

int main(int argc, char *argv[]){
    std::string line;
    std::vector<std::string> rows;
    while (std::getline(std::cin,line)) {
        rows.push_back(line);
    }

    std::vector<std::string> new_rows(rows);
    do{
        rows = new_rows;
        new_rows = simulate(rows);
        //printvector(new_rows);
    }while(new_rows != rows);


    int counter=0;
    for(int i=0;i<rows.size();i++){
        for(int j=0;j<rows[i].size();j++){
            counter += rows[i][j]=='#';
        }
    }

    printf("Used seats: %d\n",counter);
}
