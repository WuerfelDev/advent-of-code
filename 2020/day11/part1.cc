#include <iostream>
#include <vector>



void printvector(std::vector<std::string> rows){
    for(int i=0;i<rows.size();i++){
        printf("%s\n",rows[i].c_str());
    }
    printf("\n");
}


int get_adjacent(std::vector<std::string> rows,int pos_i,int pos_j){
    int ret = 0;
    for(int i=pos_i-1;i<=pos_i+1;i++){
        if(i>=0 && i<rows.size()){
            for(int j=pos_j-1;j<=pos_j+1;j++){
                if(j>=0 && j<rows[i].size()){
                    if(!(pos_i == i && pos_j == j)){
                        ret += rows[i][j]=='#';
                    }
                }
            }
        }
    }
    return ret;
}

std::vector<std::string> simulate(std::vector<std::string> rows){
    std::vector<std::string> cpy(rows);
    for(int i=0;i<rows.size();i++){
        for(int j=0;j<rows[i].size();j++){
            if(rows[i][j]!='.'){
                int adjacent = get_adjacent(rows,i,j);
                if(rows[i][j]=='L' && adjacent==0){
                    cpy[i][j] = '#';
                }else if(rows[i][j]=='#' && adjacent>=4){
                    cpy[i][j] = 'L';
                }
            }
        }
    }
    return cpy;
}

int main(int argc, char *argv[]){
    std::string line;
    std::vector<std::string> rows;
    while (std::getline(std::cin,line)) {
        rows.push_back(line);
    }

    std::vector<std::string> new_rows(rows);
    do{
        rows = new_rows;
        new_rows = simulate(rows);
        //printvector(new_rows);
    }while(new_rows != rows);


    int counter=0;
    for(int i=0;i<rows.size();i++){
        for(int j=0;j<rows[i].size();j++){
            counter += rows[i][j]=='#';
        }
    }

    printf("Used seats: %d\n",counter);
}
