#include <iostream>
#include <vector>

int main(int argc, char *argv[]){
    std::string lineInput;
    std::vector<int> storage;
    while (std::getline(std::cin,lineInput)) {
        try {
            int input = stoi(lineInput);
            storage.push_back(input);
        } catch(std::invalid_argument& e){
            printf("Error reading a number / End o file");
        }
    }   

    for(int i=0;i<storage.size();i++){
        for(int j=i+1;j<storage.size();j++){
            for(int k=j+1;k<storage.size();k++){
                if(storage[i]+storage[j]+storage[k]== 2020){
                    printf("Result: %d\n",storage[i]*storage[j]*storage[k]);
                }
            }
        }
    }

}

