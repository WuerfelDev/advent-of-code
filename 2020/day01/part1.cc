#include <iostream>
#include <vector>

int main(int argc, char *argv[]){
    std::string lineInput;
    std::vector<int> storage;
    while (std::getline(std::cin,lineInput)) {
        int input = stoi(lineInput);
        for(int i=0;i<storage.size();i++){
            if(input+storage[i]==2020){
                printf("Result: %d\n",input*storage[i]);
            }
        }
        storage.push_back(input);
    }   
}

