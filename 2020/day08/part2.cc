#include <iostream>
#include <vector>


bool in_history(std::vector<int> history, int now){
    for(int i=0;i<history.size();i++){
        if(history[i]==now){
            return true;
        }
    }
    return false;
}


int get_number(std::string command){
    return stoi(command.substr(command.find(" ")));
}


int try_to_run(std::vector<std::string> commands){
    int accumulator = 0;
    int i = 0;
    std::vector<int> history;
    bool running = true;
    while(running && !in_history(history,i)){
        history.push_back(i);
        
        std::string cmd = commands[i].substr(0,3);
        if(cmd=="nop"){
            i++;
        }else if(cmd== "acc"){
            accumulator += get_number(commands[i]);
            i++;
        }else if(cmd=="jmp"){
            i += get_number(commands[i]);
        }
        if(i==commands.size()){
            // after last line. Done.
            return accumulator;
        }
    }
    return -1;
}

int main(int argc, char *argv[]){
    std::string line;
    std::vector<std::string> commands;
    while (std::getline(std::cin,line)) {
        commands.push_back(line);
    }

    int accumulator;
    for(int i=0;i<commands.size();i++){
        if(commands[i].substr(0,3) == "jmp"){
            std::vector<std::string> new_commands = commands;
            new_commands[i] = "nop" + commands[i].substr(3);
            int ret = try_to_run(new_commands);
            if(ret!=-1){
                printf("Accumulator: %d\n", ret);
                return 0;
            }
        }else if(commands[i].substr(0,3) == "nop"){
            std::vector<std::string> new_commands = commands;
            new_commands[i] = "jmp" + commands[i].substr(3);
            int ret = try_to_run(new_commands);
            if(ret!=-1){
                printf("Accumulator: %d\n", ret);
                return 0;
            }
        }
    }

    printf("Failed\n");
}
