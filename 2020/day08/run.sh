#/bin/bash

g++ part1.cc -o part1
cat input.txt | ./part1

g++ part2.cc -o part2
cat input.txt | ./part2
