#include <iostream>
#include <vector>


bool in_history(std::vector<int> history, int now){
    for(int i=0;i<history.size();i++){
        if(history[i]==now){
            return true;
        }
    }
    return false;
}


int get_number(std::string command){
    return stoi(command.substr(command.find(" ")));
}

int main(int argc, char *argv[]){
    std::string line;
    std::vector<std::string> commands;
    while (std::getline(std::cin,line)) {
        commands.push_back(line);
    }

    int accumulator = 0;
    int i = 0;
    std::vector<int> history;
    while(!in_history(history,i)){
        history.push_back(i);
        
        std::string cmd = commands[i].substr(0,3);
        if(cmd=="nop"){
            i++;
        }else if(cmd== "acc"){
            accumulator += get_number(commands[i]);
            i++;
        }else if(cmd=="jmp"){
            i += get_number(commands[i]);
        }

    }

    printf("Accumulator: %d\n", accumulator);
}
