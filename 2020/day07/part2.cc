#include <iostream>
#include <vector>
#include <sstream>



int get_children_recursive(std::vector<std::vector<std::string>> collector, std::string name){
    for(int i=0;i<collector.size();i++){
        if(collector[i][0]==name){
            // Found the entry
            int return_value = 0;
            // Loop the children
            for(int j=1;j<collector[i].size();j++){
                int number = collector[i][j][0] - '0';
                if(number<=9 && number>=1){
                    return_value += number;
                    return_value += number * get_children_recursive(collector,collector[i][j].substr(2));
                }
                // Else wrong number -> means wasn't a 0-9 char
            }
            return return_value;
        }
    }
    // Didnt find 'name'
    printf("This is an error");
    return -1;
}


//vector:
//  0: bag name
//  *: childbags 
std::vector<std::string> get_bag(std::string line){
    std::vector<std::string> bag;
    std::size_t pos = line.find(" bags");
    bag.push_back(line.substr(0,pos));
    std::string part;
    std::istringstream ss(line.substr(pos+13));
    while(std::getline(ss, part, ',')) { 
        // Remove leading space and trailing " bag.."
        bag.push_back(part.substr(1,part.find(" bag")-1));
    }
    // If no bags contained, only one element is in the vector
    return bag;
}

int main(int argc, char *argv[]){
    std::string line;
    std::vector<std::vector<std::string>> collector;

    while (std::getline(std::cin,line)) {
        collector.push_back(get_bag(line));
    }

    int result = get_children_recursive(collector,"shiny gold");

    printf("Bags needed: %d\n", result);
}
