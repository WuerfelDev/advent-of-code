#include <iostream>
#include <vector>
#include <sstream>
#include <algorithm>


std::vector<std::string> get_parentbags(std::vector<std::vector<std::string>> collector, std::string search){
    std::vector<std::string> parents;
    // Every bag
    for(int i=0;i<collector.size();i++){
        // Every child bag (skips enties without children)
        for(int j=1;j<collector[i].size();j++){
            if(collector[i][j]==search){
                parents.push_back(collector[i][0]);
            }
        }
    }
    return parents;
}


//vector:
//  0: bag name
//  *: childbags 
std::vector<std::string> get_bag(std::string line){
    std::vector<std::string> bag;
    std::size_t pos = line.find(" bags");
    bag.push_back(line.substr(0,pos));
    std::string part;
    std::istringstream ss(line.substr(pos+13));
    while(std::getline(ss, part, ',')) { 
        // Remove leading space and trailing " bag.."
        bag.push_back(part.substr(1+2,part.find(" bag")-1-2));
    }
    // If no bags contained, only one element is in the vector
    return bag;
}

int main(int argc, char *argv[]){
    std::string line;
    std::vector<std::vector<std::string>> collector;
    while (std::getline(std::cin,line)) {
        collector.push_back(get_bag(line));
    }



    // Get "shiny gold" parents
    std::vector<std::string> bags;
    std::vector<std::string> new_bags;
    std::vector<std::string> tmp;
    tmp.push_back("shiny gold");
    do{
        // remove bags that are already in bags (duplicates)
        for(int i=0;i<new_bags.size();i++){
            if(bags.end()==std::find(bags.begin(), bags.end(), new_bags[i])){
                tmp.push_back(new_bags[i]);
                bags.push_back(new_bags[i]);
            }
        }
        new_bags.clear();
        for(int i=0;i<tmp.size();i++){
            std::vector<std::string> val = get_parentbags(collector,tmp[i]);
            if(!val.empty()){
                for(int j=0;j<val.size();j++){
                    new_bags.push_back(val[j]);
                }
            }
        }
        tmp.clear();
    }while(!new_bags.empty());

    printf("Answer: %ld\n", bags.size());
}
