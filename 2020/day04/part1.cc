#include <iostream>
#include <vector>



int validate_entry(std::string passport,std::string test){
    if(passport.find(test)==std::string::npos){
        // Failed to find the substring
        return 0;
    }
    return 1;
}


int check_valid(std::vector<std::string> passport_lines){
    std::string passport;
    int counter = 0;
    for(int i=0;i<passport_lines.size();i++){
        passport += " "+passport_lines[i];
    }

    counter += validate_entry(passport,"byr");
    counter += validate_entry(passport,"iyr");
    counter += validate_entry(passport,"eyr");
    counter += validate_entry(passport,"hgt");
    counter += validate_entry(passport,"hcl");
    counter += validate_entry(passport,"ecl");
    counter += validate_entry(passport,"pid");

    return counter==7;
}

int main(int argc, char *argv[]){
    std::string line;
    std::vector<std::string> collector;
    int valid = 0;
    while (std::getline(std::cin,line)) {
        if(line==""){
            valid += check_valid(collector);
            collector.clear();
        }else{
            collector.push_back(line);
        }
    }
    valid += check_valid(collector);

    printf("Valid passports: %d\n", valid);
}
