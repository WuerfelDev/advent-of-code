#include <iostream>
#include <vector>
#include <sstream>
#include <locale>


// https://stackoverflow.com/a/1798170/4346956
std::string trim(const std::string& str,
        const std::string& whitespace = " \t")
{
    const auto strBegin = str.find_first_not_of(whitespace);
    if (strBegin == std::string::npos){
        return ""; // no content
    }

    const auto strEnd = str.find_last_not_of(whitespace);
    const auto strRange = strEnd - strBegin + 1;

    return str.substr(strBegin, strRange);
}



int check_valid(std::vector<std::string> passport_lines){
    std::string passport;
    int counter = 0;
    for(int i=0;i<passport_lines.size();i++){
        passport += " "+passport_lines[i];
    }


    std::vector<std::string> fields;
    std::istringstream pp_stream(passport);
    std::string field;
    while (std::getline(pp_stream,field,' ')) {
        if(field.find(":")!=std::string::npos){ //Remove invalid entries (eg empty)
            std::string value = trim(field.substr(4));
            if(field.find("byr")!=std::string::npos){
                try{
                    if(value.length()==4 && stoi(value)>=1920 && stoi(value)<=2002){
                        counter++;
                    }
                }catch(std::invalid_argument& e){
                    //Do nothing
                }
            }else if(field.find("iyr")!=std::string::npos){
                try{
                    if(value.length()==4 && stoi(value)>=2010 && stoi(value)<=2020){
                        counter++;
                    }
                }catch(std::invalid_argument& e){
                    //Do nothing
                }
            }else if(field.find("eyr")!=std::string::npos){
                try{
                    if(value.length()==4 && stoi(value)>=2020 && stoi(value)<=2030){
                        counter++;
                    }
                }catch(std::invalid_argument& e){
                    //Do nothing
                }
            }else if(field.find("hgt")!=std::string::npos){
                try{
                    std::string unit = value.substr(value.length()-2);
                    std::string val = value.substr(0,value.length()-2);


                    if(unit=="cm" && stoi(val)>=150 && stoi(val)<=193){
                        counter++;
                    }else if(unit=="in" && stoi(val)>=59 && stoi(val)<=76){
                        counter++;
                    }
                }catch(std::invalid_argument& e){
                    //Do nothing
                }
            }else if(field.find("hcl")!=std::string::npos){
                if(value[0]=='#' && value.length()==7){
                    std::string valid_chars = "0123456789abcdef";
                    bool failed = false;
                    for(int i=1;i<7;i++){
                        if(valid_chars.find(value[i])==std::string::npos){
                            failed == true;
                        }
                    }
                    counter += !failed;
                }
            }else if(field.find("ecl")!=std::string::npos){
                //amb blu brn gry grn hzl oth
                std::string vals[7] = {"amb", "blu", "brn", "gry", "grn", "hzl", "oth"};
                for(int i=0;i<7;i++){
                    if(value==vals[i]){
                        counter++;
                    }
                }
            }else if(field.find("pid")!=std::string::npos){
                if(value.length()==9){
                    bool failed = false;
                    for(int i=0;i<9;i++){
                        if(!std::isdigit(value[i])){
                            failed = true;
                        }
                    }
                    counter += !failed;
                }
            } 
        }
    }

    return counter==7;
}

int main(int argc, char *argv[]){
    std::string line;
    std::vector<std::string> collector;
    int valid = 0;
    while (std::getline(std::cin,line)) {
        if(line==""){
            valid += check_valid(collector);
            collector.clear();
        }else{
            collector.push_back(line);
        }
    }
    valid += check_valid(collector);

    printf("Valid passports: %d\n", valid);
}
