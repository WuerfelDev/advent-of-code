#include <iostream>
#include <sstream>

int main(int argc, char *argv[]){
    std::string lineInput;
    std::string tmp;
    int isvalid = 0;
    while (std::getline(std::cin,lineInput)) {
        std::istringstream line(lineInput);

        // 13-17
        std::getline(line, tmp, ' ');
        std::istringstream minmax(tmp);
        std::getline(minmax, tmp, '-');
        int minv = stoi(tmp);
        std::getline(minmax, tmp, '-');
        int maxv = stoi(tmp);

        // s:
        std::getline(line, tmp, ' ');
        char c = tmp[0];

        // ssssssssssssgsssj
        std::getline(line, tmp, ' ');

        int counter = 0;
        for(int i=0;i<tmp.size();i++){
            if(tmp[i]==c){
                counter++;
            }
        }
        if(counter >= minv && counter <= maxv){
            isvalid++;
        }

    }
    printf("valid passwords: %d\n", isvalid);
}
