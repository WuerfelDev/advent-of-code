#!/bin/env bash

# Argument $1: input file


counter=0
last_out=0

# a,b,c groups
ctra=0
ctrb=0
ctrc=0
# indicator which group completes
line_ptr=0

# helper to skip first lines output calculation
begin=0;
while read line; do
    ctra=$((line+ctra))
    ctrb=$((line+ctrb))
    ctrc=$((line+ctrc))
    out=0
    case $line_ptr in
        0)
            if [ "$begin" -gt "0" ]
            then
                out=$ctra
                ctra=0
            fi
            ;;
        1) 
            if [ "$begin" -eq "0" ]
            then
                ctrb=$line 
            else
                out=$ctrb
                ctrb=0
            fi
            ;;
        2)
            if [ "$begin" -eq "0" ]
            then
                ctrc=$line 
                begin=1
            else
                out=$ctrc
                ctrc=0
            fi
            ;;
    esac
    # keep variable as 0,1,2
    line_ptr=$(((line_ptr+1)%3))

    if [ "$out" -gt "0" ] && [ "$begin" -gt "0" ]
    then
        if [ "$out" -gt "$last_out" ]
        then
            counter=$((counter+1))
        fi
        last_out=$out
    fi
done < $1
echo "The result is : $counter"
