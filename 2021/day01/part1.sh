#!/bin/env bash

# Argument $1: input file

counter=0
last_line=""
while read line; do
    if [ -n "$last_line" ]
    then
        if [ "$line" -gt "$last_line" ]
        then
            counter=$((counter+1))
        fi
    fi
    last_line=$line
done < $1
echo "The result is : $counter"
