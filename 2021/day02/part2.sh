#!/bin/env bash

# Argument $1: input file

horiz=0
depth=0
aim=0

while read line; do
    arr=($line)
    case ${arr[0]} in
        down)
            aim=$((aim+ arr[1]))
            ;;
        up)
            aim=$((aim - arr[1]))
            ;;
        forward)
            horiz=$((horiz + arr[1]))
            depth=$((depth + aim * arr[1]))
            ;;
    esac
done < $1

echo "The result is: $((horiz * depth))"
