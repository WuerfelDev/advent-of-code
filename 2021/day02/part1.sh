#!/bin/env bash

# Argument $1: input file

horiz=0
depth=0

while read line; do
    arr=($line)
    case ${arr[0]} in
        down)
            depth=$((depth + arr[1]))
            ;;
        up)
            depth=$((depth - arr[1]))
            ;;
        forward)
            horiz=$((horiz + arr[1]))
            ;;
    esac
done < $1

echo "The result is: $((horiz * depth))"
