# Advent of Code
These are my solutions to the daily challenges.

Link: [Advent of Code](https://adventofcode.com)


## Configuration for aoc-cli

### Setup

AoC cli: https://github.com/scarvalhojr/aoc-cli

```bash
echo "<session_cookie>" > ./.adventofcode.session
ln -s $PWD/.adventofcode.session ~
```

### Usage

```bash
aoc --year 2015 --day 1 # Show info
aoc --year 2015 --day 1 -I d # save input file

```
